var glob = require("glob");

function toObject(paths) {
    var ret = {};

    paths.forEach(function(path) {
        // you can define entry names mapped to [name] here
        var pathParts = path.split('/');
        var filename = pathParts.slice(-1)[0];
        var appName = pathParts.slice(-3)[0];
        var buildDestination = appName + '/' + filename;
        ret[buildDestination] = path;
    });

    return ret;
}

const config = {
    entry: toObject(glob.sync('./!(node_modules)/react/*.js')),
    output: {
        filename: "[name]",
        path: __dirname + "/static/dist"
    },
    mode: "development",

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015', 'react']
                    }
                }
            }
        ]
    }
};

module.exports = config;
