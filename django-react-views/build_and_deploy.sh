#!/usr/bin/env bash

rm -r build/*
rm -r dist/*
rm -r channels_redux.egg-info/*

python setup.py sdist bdist_wheel

REPO_URL="https://test.pypi.org/legacy/"
if [ "$1" == "--real" ]
then
    REPO_URL="https://upload.pypi.org/legacy/"
fi
twine upload --repository-url $REPO_URL dist/*
