from django.db import models


class ExampleModel(models.Model):
    example_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
