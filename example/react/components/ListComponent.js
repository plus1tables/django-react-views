import React from 'react'
import { render } from 'react-dom'

function do_image_search(q) {
    var api_key = "AIzaSyBR6yro8cs-MW-E4RsYW6q0X5lDki2X5cE";
    var cx = '010772045683509949517:md7qcrgegtk';
    var url = "https://www.googleapis.com/customsearch/v1?num=1&key=" + api_key + "&cx=" + cx + "&q=fruit+" + q;

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
            let li = document.getElementById("fruit_" + q);
            let img = document.createElement('img');
            img.src = JSON.parse(xmlHttp.responseText).items[0].pagemap.cse_image[0].src;
            img.style.maxWidth = "500px";
            img.title = q;
            li.innerHTML = '';
            li.appendChild(img);
        }
    };
    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
    return null;
}

const ListComponent = ({objects}) =>
    <React.Fragment>
        <h1>{objects.length} Random Fruits</h1>
        <ul style={{listStyle: "none"}}>
            { objects.map((object => <li id={`fruit_${object.name}`} key={object.pk}>{object.name}{do_image_search(object.name)}</li>)) }
        </ul>
    </React.Fragment>;

export default ListComponent;
