import React from 'react'
import { render } from 'react-dom'
import ListComponent from "./components/ListComponent";

let objects = Object.values(window.props.objects['example.examplemodel']);

render(
    <ListComponent objects={objects}/>,
    window.react_mount
);
