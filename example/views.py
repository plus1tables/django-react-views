from django_react_views.views import ReactTemplateView, ReactListView, ModelSerializer
from example.models import ExampleModel


class BasicSerializer(ModelSerializer):
    """
    This is for example purposes only, not suitable for production.
    This is used here to avoid the need to bring in DjangoRestFramework and simplify the example code
    """

    # noinspection PyMissingConstructor
    def __init__(self, instance, context, many):
        self.instance = instance
        self.context = context
        self.many = many

    def _serialize_object(self, single_object):
        return {
            'pk': single_object.pk,
            'name': single_object.name
        }

    @property
    def data(self):
        if self.many:
            return [self._serialize_object(obj) for obj in self.instance]
        else:
            return self._serialize_object(self.instance)


class Index(ReactTemplateView):
    react_component = 'HomePage.js'
    template_name = 'example/index.html'


class ListView(ReactListView):
    react_component = "ListPage.js"
    template_name = 'example/list.html'
    queryset = ExampleModel.objects.order_by('?')[:5]
    model_serializer = BasicSerializer
    key_field = 'pk'  # Default is url, meant to be used with DjangoRestFramework's HyperLinkedModelSerializer
